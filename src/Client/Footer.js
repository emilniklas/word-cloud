import React from 'react'

export function Footer () {
  return (
    <footer className='Footer'>
      <address className='Footer__contact'>
        Emil Persson<br />
        emil.n.persson@gmail.com
      </address>
    </footer>
  )
}
