import { Observable } from 'rxjs/Observable'
import 'rxjs/add/Observable/empty'
import Parser from 'rss-parser'

export class RssSource {
  constructor (parser, urls) {
    this._parser = parser
    this._urls = urls
  }

  static make (urls) {
    return new RssSource(
      new Parser(),
      urls
    )
  }

  words () {
    if (this._urls.length === 0) {
      return Observable.empty()
    }

    return Observable.create(async observer => {
      try {
        await Promise.all(this._urls.map(async url => {
          const { items } = await this._parser.parseURL(url)

          for (const item of items) {
            observer.next(item.title)
          }
        }))
      } catch (e) {
        observer.error(e)
      } finally {
        observer.complete()
      }
    })
  }
}
