import React from 'react'
import { WordCloud } from './WordCloud'
import { Header } from './Header'
import { Footer } from './Footer'

export function Root () {
  return (
    <div className='Root'>
      <Header />
      <WordCloud />
      <Footer />
    </div>
  )
}
