import { createServer } from 'http'
import { WordCounter } from './WordCloud/WordCounter'
import { TwitterSource } from './WordCloud/TwitterSource'
import { RssSource } from './WordCloud/RssSource'
import { CompositeSource } from './WordCloud/CompositeSource'
import { TransformerSource } from './WordCloud/TransformerSource'
import * as Transformers from './WordCloud/Transformers'
import * as url from 'url'

createServer(async (req, res) => {
  const uri = url.parse(req.url, true)

  try {
    switch (uri.pathname) {
      case '/api/cloud':
        const wordCounter = new WordCounter(
          new TransformerSource(
            new CompositeSource([
              TwitterSource.make([].concat(uri.query.hashtag || [])),
              RssSource.make([].concat(uri.query.rss || []))
            ]),
            Transformers.compose(
              Transformers.lowercase,
              Transformers.removeUrls,
              Transformers.removeStopWords([].concat(uri.query.stopword || [])),
              Transformers.removePunctuation,
              Transformers.removeOneLetterWords
            )
          )
        )

        const stats = await wordCounter.count()

        res.end(JSON.stringify(stats))
        break

      default:
        res.writeHead(404)
        res.end(JSON.stringify({ message: 'Not Found' }))
        break
    }
  } catch (e) {
    console.error(e.stack || e)
    res.writeHead(500)
    res.end(JSON.stringify({ message: 'Server Error' }))
  }
}).listen(80)
