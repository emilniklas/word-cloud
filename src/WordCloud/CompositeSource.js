import { Observable } from 'rxjs/Observable'
import 'rxjs/add/Observable/empty'
import 'rxjs/add/operator/merge'

export class CompositeSource {
  constructor (sources) {
    this._sources = sources
  }

  words () {
    if (this._sources.length === 0) {
      return Observable.empty()
    }

    return this._sources.reduce(
      (a, b) => a.words().merge(b.words())
    )
  }
}
