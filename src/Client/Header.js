import React from 'react'

export function Header () {
  return (
    <header className='Header'>Tag Cloud by Emil Persson</header>
  )
}
