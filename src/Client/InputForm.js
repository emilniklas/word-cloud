import React from 'react'

export function InputForm ({ form, onChange }) {
  const onChangeForm = patch => {
    onChange(Object.assign({}, form, patch))
  }

  const onChangeHashtags = event => {
    onChangeForm({
      hashtags: event.target.value.split(',').map(t => t.trim())
    })
  }

  const onChangeRssLinks = event => {
    onChangeForm({
      rssLinks: event.target.value.split(',').map(t => t.trim())
    })
  }

  const onChangeStopWords = event => {
    onChangeForm({
      stopWords: event.target.value.split(',').map(t => t.trim())
    })
  }

  return (
    <dl className='InputForm'>
      <label>
        <dt>Twitter Hashtags</dt>
        <dd><input type='text' value={form.hashtags.join(',')} onChange={onChangeHashtags} /></dd>
      </label>

      <label>
        <dt>RSS Links</dt>
        <dd><input type='text' value={form.rssLinks.join(',')} onChange={onChangeRssLinks} /></dd>
      </label>

      <label>
        <dt>Stop Words</dt>
        <dd><textarea value={form.stopWords.join(',')} onChange={onChangeStopWords} /></dd>
      </label>
    </dl>
  )
}
