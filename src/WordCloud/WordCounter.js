import 'rxjs/add/operator/map'
import 'rxjs/add/operator/mergeMap'

export class WordCounter {
  constructor (source) {
    this._source = source
  }

  async count () {
    const words = {}

    await this._source
      .words()
      .mergeMap(this._splitPhraseIntoWords)
      .forEach(this._addWordToMap(words))

    return Object.keys(words)
      .sort((wordA, wordB) => {
        const aOccurrences = words[wordA]
        const bOccurrences = words[wordB]

        if (aOccurrences > bOccurrences) {
          return -1
        }

        if (aOccurrences < bOccurrences) {
          return 1
        }

        return 0
      })
      .slice(0, 100)
      .reduce((o, w) => Object.assign(o, { [w]: words[w] }), {})
  }

  _splitPhraseIntoWords (phrase) {
    return phrase.split(/\s+/g)
  }

  _addWordToMap (map) {
    return word => {
      const current = map[word]

      map[word] = current == null ? 1 : current + 1
    }
  }
}
