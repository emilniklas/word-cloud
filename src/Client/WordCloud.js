import React from 'react'
import { InputForm } from './InputForm'
import * as Api from './Api'

export class WordCloud extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      loading: false,
      form: {
        hashtags: [],
        rssLinks: [],
        stopWords: []
      },
      data: {}
    }
  }

  componentDidMount () {
    this._setFormState({
      hashtags: ['javascript', 'react'],
      rssLinks: ['https://www.reddit.com/.rss'],
      stopWords: [
        'a', 'about', 'above', 'above', 'across', 'after',
        'afterwards', 'again', 'against', 'all', 'almost',
        'alone', 'along', 'already', 'also', 'although',
        'always', 'am', 'among', 'amongst', 'amoungst',
        'amount', 'an', 'and', 'another', 'any', 'anyhow',
        'anyone', 'anything', 'anyway', 'anywhere', 'are',
        'around', 'as', 'at', 'back', 'be', 'became',
        'because', 'become', 'becomes', 'becoming', 'been',
        'before', 'beforehand', 'behind', 'being', 'below',
        'beside', 'besides', 'between', 'beyond', 'bill',
        'both', 'bottom', 'but', 'by', 'call', 'can',
        'cannot', 'cant', 'co', 'con', 'could', 'couldnt',
        'cry', 'de', 'describe', 'detail', 'do', 'does', 'done',
        'down', 'due', 'during', 'each', 'eg', 'eight',
        'either', 'eleven', 'else', 'elsewhere', 'empty',
        'enough', 'etc', 'even', 'ever', 'every', 'everyone',
        'everything', 'everywhere', 'except', 'few',
        'fifteen', 'fify', 'fill', 'find', 'fire', 'first',
        'five', 'for', 'former', 'formerly', 'forty',
        'found', 'four', 'from', 'front', 'full', 'further',
        'get', 'give', 'go', 'had', 'has', 'hasnt', 'have',
        'he', 'hence', 'her', 'here', 'hereafter', 'hereby',
        'herein', 'hereupon', 'hers', 'herself', 'him',
        'himself', 'his', 'how', 'however', 'hundred', 'ie',
        'if', 'in', 'inc', 'indeed', 'interest', 'into', 'is',
        'it', 'its', 'itself', 'keep', 'last', 'latter',
        'latterly', 'least', 'less', 'like', 'ltd', 'made', 'many',
        'may', 'me', 'meanwhile', 'might', 'mill', 'mine',
        'more', 'moreover', 'most', 'mostly', 'move', 'much',
        'must', 'my', 'myself', 'name', 'namely', 'neither',
        'never', 'nevertheless', 'next', 'nine', 'no', 'nobody',
        'none', 'noone', 'nor', 'not', 'nothing', 'now',
        'nowhere', 'of', 'off', 'often', 'on', 'once', 'one',
        'only', 'onto', 'or', 'other', 'others', 'otherwise',
        'our', 'ours', 'ourselves', 'out', 'over', 'own', 'part',
        'per', 'perhaps', 'please', 'put', 'rather', 're', 'same',
        'see', 'seem', 'seemed', 'seeming', 'seems', 'serious',
        'several', 'she', 'should', 'show', 'side', 'since',
        'sincere', 'six', 'sixty', 'so', 'some', 'somehow',
        'someone', 'something', 'sometime', 'sometimes', 'somewhere',
        'still', 'such', 'system', 'take', 'ten', 'than',
        'that', 'the', 'their', 'them', 'themselves', 'then',
        'thence', 'there', 'thereafter', 'thereby', 'therefore',
        'therein', 'thereupon', 'these', 'they', 'thickv', 'thin',
        'third', 'this', 'those', 'though', 'three', 'through',
        'throughout', 'thru', 'thus', 'to', 'together', 'too',
        'top', 'toward', 'towards', 'twelve', 'twenty', 'two',
        'un', 'under', 'until', 'up', 'upon', 'us', 'very', 'via',
        'was', 'we', 'well', 'were', 'what', 'whatever', 'when',
        'whence', 'whenever', 'where', 'whereafter', 'whereas',
        'whereby', 'wherein', 'whereupon', 'wherever', 'whether',
        'which', 'while', 'whither', 'who', 'whoever', 'whole',
        'whom', 'whose', 'why', 'will', 'with', 'within', 'without',
        'would', 'yet', 'you', 'your', 'yours', 'yourself', 'yourselves', 'the'
      ]
    })
  }

  _setFormState (form) {
    this.setState({
      loading: true,
      form
    }, this._postponeSearch.bind(this))
  }

  _postponeSearch () {
    if (this._postponedSearchTimeout != null) {
      clearTimeout(this._postponedSearchTimeout)
    }

    this._postponedSearchTimeout = setTimeout(this._search.bind(this), 800)
  }

  async _search () {
    this.setState({
      loading: false,
      data: await Api.get(`/api/cloud?${this._apiQueryParams}`)
    })
  }

  get _apiQueryParams () {
    const identity = v => v
    return [
      ...this.state.form.hashtags.filter(identity).map(h => `hashtag=${encodeURIComponent(h)}`),
      ...this.state.form.rssLinks.filter(identity).map(h => `rss=${encodeURIComponent(h)}`),
      ...this.state.form.stopWords.filter(identity).map(h => `stopword=${encodeURIComponent(h)}`)
    ].join('&')
  }

  /** @see https://stackoverflow.com/questions/6274339/how-can-i-shuffle-an-array */
  _shuffle (array) {
    const copy = array.slice()

    for (let i = copy.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1))

      const tmp = copy[i]
      copy[i] = copy[j]
      copy[j] = tmp
    }

    return copy
  }

  render () {
    return (
      <main>
        <InputForm form={this.state.form} onChange={this._setFormState.bind(this)} />

        {this.state.loading ? (
          <div>Loading...</div>
        ) : (
          <ul className='WordCloud__word-list'>
            {this._shuffle(Object.keys(this.state.data)).map(this._renderWord.bind(this))}
          </ul>
        )}
      </main>
    )
  }

  get _maxOccurences () {
    return Object.values(this.state.data).reduce((a, b) => Math.max(a, b), 0)
  }

  get _minOccurences () {
    return Object.values(this.state.data).reduce((a, b) => Math.min(a, b)) || 0
  }

  _renderWord (word) {
    const occurences = this.state.data[word]
    const ratio = (occurences - this._minOccurences) / this._maxOccurences

    return (
      <li
        className='WordCloud__word'
        key={word}
        title={`${occurences} occurences`}
        style={{
          fontSize: (2 + ratio * 5) + 'vw'
        }}
      >{word}</li>
    )
  }
}
