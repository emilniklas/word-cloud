import { Observable } from 'rxjs/Observable'
import 'rxjs/add/Observable/empty'
import Twitter from 'twitter'

export class TwitterSource {
  constructor (twitter, hashtags) {
    this._twitter = twitter
    this._hashtags = hashtags
  }

  static make (hashtags) {
    return new TwitterSource(
      new Twitter({
        'consumer_key': process.env.TWITTER_CONSUMER_KEY,
        'consumer_secret': process.env.TWITTER_CONSUMER_SECRET,
        'access_token_key': process.env.TWITTER_ACCESS_TOKEN_KEY,
        'access_token_secret': process.env.TWITTER_ACCESS_TOKEN_SECRET
      }),
      hashtags
    )
  }

  words () {
    if (this._hashtags.length === 0) {
      return Observable.empty()
    }

    return Observable.create(this._pushTweetsForEveryHashtagToObserver.bind(this))
  }

  async _pushTweetsForEveryHashtagToObserver (observer) {
    await Promise.all(this._hashtags.map(this._pushTweetsForHashtag.bind(this, observer)))

    observer.complete()
  }

  async _pushTweetsForHashtag (observer, hashtag) {
    try {
      const tweets = await this._getTweetsForHashtag(hashtag)

      for (const tweet of tweets) {
        observer.next(tweet.text)
      }
    } catch (e) {
      observer.error(e)
    }
  }

  _getTweetsForHashtag (hashtag) {
    return new Promise((resolve, reject) => {
      this._twitter.get('search/tweets', { q: `#${hashtag}`, count: 100 }, (error, tweets) => {
        if (error) {
          reject(error)
        } else {
          resolve(tweets.statuses)
        }
      })
    })
  }
}
