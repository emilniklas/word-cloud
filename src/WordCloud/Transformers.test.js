import { compose, removePunctuation, lowercase, removeStopWords, removeUrls, removeOneLetterWords } from './Transformers'

describe('removePunctuation', () => {
  it("removes everything that isn't a letter or a dash", () => {
    expect(removePunctuation('')).toBe('')
    expect(removePunctuation('.')).toBe('')
    expect(removePunctuation('x')).toBe('x')
    expect(removePunctuation('x!')).toBe('x')
    expect(removePunctuation('x!15')).toBe('x')
    expect(removePunctuation('x-y!15')).toBe('x-y')
    expect(removePunctuation("x'y!15")).toBe('xy')
    expect(removePunctuation("this! isn't! cool!")).toBe('this isnt cool')
  })

  it('normalizes whitespace', () => {
    expect(removePunctuation(' ')).toBe('')
    expect(removePunctuation('x ')).toBe('x')
    expect(removePunctuation('x x')).toBe('x x')
    expect(removePunctuation('x.x')).toBe('x x')
    expect(removePunctuation('x-x. x')).toBe('x-x x')
  })

  it('removes dashes used for punctuation as opposed to contracted words', () => {
    expect(removePunctuation('awesome-sauce - run-of-the-mill -- cool'))
      .toBe('awesome-sauce run-of-the-mill cool')
  })
})

describe('lowercase', () => {
  it('makes a string lowercase', () => {
    expect(lowercase('Xx')).toBe('xx')
  })
})

describe('removeStopWords', () => {
  it('removes a list of stop words from the string', () => {
    const transformer = removeStopWords(['the', 'a', 'to'])
    expect(transformer('a present to give to the friend')).toBe(' present  give   friend')
  })

  it('only removes full words', () => {
    const transformer = removeStopWords(['at', 'the'])
    expect(transformer('at the hat store over there')).toBe('  hat store over there')
  })
})

describe('compose', () => {
  it('creates a transformer from a list of transformers', () => {
    const transformer = compose(
      lowercase,
      removeStopWords(['this', 'is']),
      removePunctuation
    )

    expect(transformer('This is GREAT fun!')).toBe('great fun')
  })
})

describe('removeUrls', () => {
  it('does nothing to most text', () => {
    expect(removeUrls('This is a phrase!')).toBe('This is a phrase!')
  })

  it('removes HTTP URL-looking things', () => {
    expect(removeUrls('Check this out: http://some-url so cool')).toBe('Check this out:  so cool')
  })

  it('removes HTTPS URL-looking things', () => {
    expect(removeUrls('Check this out: https://some-url so cool')).toBe('Check this out:  so cool')
  })
})

describe('removeOneLetterWords', () => {
  it('removes words with only one letter', () => {
    expect(removeOneLetterWords('abc a bc b c')).toBe('abc  bc  ')
  })
})
