export class StubSource {
  constructor (observable) {
    this._observable = observable
  }

  words () {
    return this._observable
  }
}
