import { RssSource } from './RssSource'
import 'rxjs/add/Observable/from'
import 'rxjs/add/operator/toArray'

describe('RssSource', () => {
  test('given no RSS URLs, it returns an empty observable', async () => {
    const parser = new MockParser()
    const source = new RssSource(parser, [])

    expect(await source.words().toArray().toPromise()).toEqual([])
  })

  test('given an RSS URL, it returns all the titles', async () => {
    const parser = new MockParser()
    const source = new RssSource(parser, ['some-rss-url'])

    expect(await source.words().toArray().toPromise()).toEqual(['This is a title', 'This is another title'])

    parser.expectUrlWasParsed('some-rss-url')
  })

  test('given two RSS URLs, it returns all the titles', async () => {
    const parser = new MockParser()
    const source = new RssSource(parser, ['some-rss-url', 'another-rss-url'])

    expect(await source.words().toArray().toPromise()).toEqual(['This is a title', 'This is another title', 'This is a title', 'This is another title'])

    parser.expectUrlWasParsed('some-rss-url')
    parser.expectUrlWasParsed('another-rss-url')
  })
})

class MockParser {
  constructor () {
    this._parsedTimes = 0
    this._parsedUrls = []
  }

  async parseURL (url) {
    this._parsedTimes++
    this._parsedUrls.push(url)

    return {
      items: [{
        title: 'This is a title'
      }, {
        title: 'This is another title'
      }]
    }
  }

  expectUrlWasParsed (url) {
    for (let i = 0; i < this._parsedTimes; i++) {
      try {
        expect(this._parsedUrls[i]).toBe(url)
        return
      } catch (e) {
        continue
      }
    }

    throw new Error(`Parser was never called with .parseURL("${url}")`)
  }
}
