import { CompositeSource } from './CompositeSource'
import { StubSource } from './StubSource'
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/Observable/from'
import 'rxjs/add/operator/toArray'

describe('CompositeSource', () => {
  it('merges the streams from two separate sources', async () => {
    const source = new CompositeSource([
      new StubSource(Observable.from(['one', 'two', 'three'])),
      new StubSource(Observable.from(['uno', 'dos', 'tres']))
    ])

    expect(await source.words().toArray().toPromise()).toEqual(['one', 'two', 'three', 'uno', 'dos', 'tres'])
  })

  it('produces an empty stream when no sources are provided', async () => {
    const source = new CompositeSource([])

    expect(await source.words().toArray().toPromise()).toEqual([])
  })
})
