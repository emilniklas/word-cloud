export function removePunctuation (phrase) {
  return phrase
    .replace(/'/g, '')
    .replace(/[^a-z-]/ig, ' ')
    .replace(/\s-+|-+\s/g, ' ')
    .replace(/\s+/g, ' ')
    .trim()
}

export function lowercase (phrase) {
  return phrase.toLowerCase()
}

export function removeStopWords (stopWords) {
  const stopWordPatterns = stopWords
    .map(stopWord => stopWord.replace(/[^a-z]/gi, ''))
    .map(stopWord => new RegExp(`\\b${stopWord}\\b`))

  return phrase => stopWordPatterns
    .reduce(
      (phrase, stopWord) => phrase.split(stopWord).join(''),
      phrase
    )
}

export function removeUrls (phrase) {
  return phrase
    .replace(/https?:\/\/\S*/g, '')
}

export function removeOneLetterWords (phrase) {
  return phrase
    .replace(/\b\w\b/g, '')
}

export function compose (...transformers) {
  return transformers
    .reduce((inner, transformer) =>
      phrase => transformer(inner(phrase)),
      phrase => phrase
    )
}
