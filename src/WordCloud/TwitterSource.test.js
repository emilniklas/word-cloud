import { TwitterSource } from './TwitterSource'
import 'rxjs/add/Observable/from'
import 'rxjs/add/operator/toArray'

describe('TwitterSource', () => {
  test('given no hashtags, it returns no words', async () => {
    const source = new TwitterSource(
      new MockTwitterClient(),
      []
    )

    expect(await source.words().toArray().toPromise()).toEqual([])
  })

  test('given one hashtag, it searches twitter using the client', async () => {
    const client = new MockTwitterClient()
    const source = new TwitterSource(client, ['hashtag'])

    expect(await source.words().toArray().toPromise()).toEqual(['This is a Tweet!', 'This is another'])
    client.expectGetWasCalledWith('search/tweets', { q: '#hashtag', count: 100 })
  })

  test('given two hashtags, it searches twitter twice', async () => {
    const client = new MockTwitterClient()
    const source = new TwitterSource(client, ['hashtag', 'another'])

    expect(await source.words().toArray().toPromise()).toEqual(
      ['This is a Tweet!', 'This is another', 'This is a Tweet!', 'This is another']
    )
    client.expectGetWasCalledWith('search/tweets', { q: '#hashtag', count: 100 })
    client.expectGetWasCalledWith('search/tweets', { q: '#another', count: 100 })
  })
})

class MockTwitterClient {
  constructor () {
    this._getCalledTimes = 0
    this._getUris = []
    this._getArgs = []
  }

  get (uri, args, callback) {
    this._getCalledTimes++
    this._getUris.push(uri)
    this._getArgs.push(args)

    callback(null, {
      statuses: [{
        text: 'This is a Tweet!'
      }, {
        text: 'This is another'
      }]
    })
  }

  expectGetWasCalledWith (uri, args) {
    for (let i = 0; i < this._getCalledTimes; i++) {
      try {
        expect(uri).toBe(this._getUris[i])
        expect(args).toEqual(this._getArgs[i])
        return
      } catch (e) {
        continue
      }
    }

    throw new Error(`Client was never called with .get("${uri}", ${JSON.stringify(args)})`)
  }
}
