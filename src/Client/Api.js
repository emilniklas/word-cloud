export async function request (method, path) {
  const response = await fetch(path, { method })

  return response.json()
}

export const get = request.bind(null, 'GET')
