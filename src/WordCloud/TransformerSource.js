import 'rxjs/add/operator/map'

export class TransformerSource {
  constructor (source, transformer) {
    this._source = source
    this._transformer = transformer
  }

  words () {
    return this._source
      .words()
      .map(this._transformer)
  }
}
