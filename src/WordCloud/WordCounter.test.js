import { WordCounter } from './WordCounter'
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/Observable/empty'
import 'rxjs/add/Observable/from'
import { StubSource } from './StubSource'

describe('WordCounter', () => {
  test('given an empty stream, when counted, then results in an empty map', async () => {
    const counter = new WordCounter(new StubSource(Observable.empty()))

    const words = await counter.count()

    expect(words).toEqual({})
  })

  test('given a single word, when counted, then reports the correct count', async () => {
    const counter = new WordCounter(new StubSource(Observable.from(['hello'])))

    const words = await counter.count()

    const expected = {
      'hello': 1
    }

    expect(words).toEqual(expected)
  })

  test('given multiple words in a single chunk, when counted, then reports all words', async () => {
    const counter = new WordCounter(new StubSource(Observable.from(['hello', 'friends'])))

    const words = await counter.count()

    const expected = {
      'hello': 1,
      'friends': 1
    }

    expect(words).toEqual(expected)
  })

  test('given multiple words in multiple chunks, when counted, the reports all words', async () => {
    const counter = new WordCounter(new StubSource(Observable.from(['hello there', 'my friends'])))

    const words = await counter.count()

    const expected = {
      'hello': 1,
      'there': 1,
      'my': 1,
      'friends': 1
    }

    expect(words).toEqual(expected)
  })

  test('given the same word multiple times, when counted, then reports each occurence', async () => {
    const counter = new WordCounter(new StubSource(Observable.from(['hello hello', 'hello'])))

    const words = await counter.count()

    const expected = {
      'hello': 3
    }

    expect(words).toEqual(expected)
  })

  test('integration', async () => {
    const counter = new WordCounter(new StubSource(Observable.from([
      'lorem ipsum dolor sit amet ipsum dolor',
      'ipsum lorem sit amet',
      'lorem lorem lorem lorem'
    ])))

    const words = await counter.count()

    const expected = {
      'amet': 2,
      'dolor': 2,
      'ipsum': 3,
      'lorem': 6,
      'sit': 2
    }

    expect(words).toEqual(expected)
  })

  test('it returns only the 100 most used words', async () => {
    let words = []

    for (let i = 1; i <= 101; i++) {
      words.push(`word-repeated-${i}-times `.repeat(i).slice(0, -1))
    }

    const counter = new WordCounter(new StubSource(Observable.from(words)))

    const result = await counter.count()

    expect(result).not.toEqual(
      expect.objectContaining({ 'word-repeated-1-times': 1 })
    )
  })
})
