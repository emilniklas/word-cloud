import { Observable } from 'rxjs/Observable'
import 'rxjs/add/Observable/from'
import 'rxjs/add/operator/toArray'
import { TransformerSource } from './TransformerSource'
import { StubSource } from './StubSource'

describe('TransformerSource', () => {
  it('it decorates a source and applies a function to each element from that source', async () => {
    const double = x => x * 2

    const source = new TransformerSource(new StubSource(Observable.from([1, 2, 3])), double)

    expect(await source.words().toArray().toPromise()).toEqual([2, 4, 6])
  })
})
